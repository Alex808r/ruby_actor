# frozen_string_literal: true

puts '=' * 20

main = Ractor.new name: 'main' do
  sleep 2
  42
end

secondary = Ractor.new name: 'secondary' do
  sleep 1
  100
end

ractors = [main, secondary]

while ractors.any?
  ract, result = Ractor.select(*ractors)
  puts ract.inspect
  puts '=' * 20
  puts result

  ractors.delete(ract)
end
