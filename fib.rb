# frozen_string_literal: true

puts '=' * 20

def fib(n)
  return n if n < 2

  fib(n - 2) + fib(n - 1)
end

main = Ractor.new name: 'main' do
  loop do
    Ractor.yield(Ractor.receive)
  end
end

CORES = 8

(1..CORES).map do |i|
  Ractor.new(main, i) do |main_ractor, index|
    loop do
      num = main_ractor.take

      result = fib(num)

      # sleep rand(0..4)

      puts "Num: #{num} Fibonachi result: #{result} from ractor #{index + 1}"
    end
  end
end

loop do
  puts 'Enter a number: '
  num = gets
  main.send num.to_i
end
