# frozen_string_literal: true

puts '=' * 20

main = Ractor.new name: 'main' do
  loop do
    Ractor.yield(Ractor.receive)
  end
end

CORES = 8

(1..CORES).map do |i|
  Ractor.new(main, i) do |main_ractor, index|
    loop do
      num = main_ractor.take

      sleep rand(0..4)

      puts "Number #{num} from ractor #{index + 1}"
    end
  end
end

loop do
  puts 'Enter a number: '
  num = gets
  main.send num.to_i
end
