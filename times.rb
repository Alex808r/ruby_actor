# frozen_string_literal: true

puts '=' * 20

main = Ractor.new name: 'main' do
  loop do
    Ractor.yield(Ractor.receive)
  end
end

secondary_racts = 5.times.map do |i|
  Ractor.new(main, i) do |main_ract, index|
    delay = rand(0..3)
    sleep delay / 5.to_f
    main_ract.send("Hi from ractor #{index + 1}")
  end
end

puts secondary_racts

5.times do
  puts main.take
end
