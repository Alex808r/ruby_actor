# frozen_string_literal: true

ract = Ractor.new name: 'main' do
  a = 42
end

puts ract.name
# => main

puts ract.take
# => 42

#################################
b = 10

ract2 = Ractor.new(b) do |val|
  a = 42 + val
end

puts ract2.take
# => 52

#################################
b = 20

ract3 = Ractor.new do
  val = Ractor.receive
  a = 42 + val
end

ract3.send(b)
puts ract3.take
# => 62
